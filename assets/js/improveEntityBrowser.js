/**
 * @file
 * OGF Paragraph behaviors.
 */
(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.improveEntityBrowser = {
    attach: function (context, settings) {
      once('trTriggerOnce', '.views-table tbody tr', context).forEach( (el) => {
        let tdClass = 'views-field-entity-browser-select'; //Classes for td
        let inputSelector = `.${tdClass} input`; //Selector for input in the td

        $(el).on('click', 'td', function (e) {
          // If the td has the class, it's because the user click on the input
          if($(this).hasClass(tdClass)) {
            return;
          }
          if($(this).find('a').length) {
            return;
          }
          let trParent = $(this).parent('tr');
          e.preventDefault();
          e.stopPropagation();

          // If the user click on the td, we trigger the click on the input
          let input = $(trParent).find(inputSelector);
          input.prop('checked', !input.prop('checked'));
        });
      });
    }
  };

})(jQuery, Drupal, drupalSettings);
