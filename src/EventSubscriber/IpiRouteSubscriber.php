<?php

declare(strict_types=1);

namespace Drupal\ipi\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\ipi\Controller\ParagraphsBrowserController;
use Symfony\Component\Routing\RouteCollection;

/**
 * Route subscriber.
 */
final class IpiRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    if ($route = $collection->get('paragraphs_browser.paragraphs_browser_controller')) {
      $route->setDefault('_controller', ParagraphsBrowserController::class . '::paragraphsBrowserSelect');
    }
  }

}
