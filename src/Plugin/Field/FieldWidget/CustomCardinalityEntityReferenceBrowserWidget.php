<?php

namespace Drupal\ipi\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\entity_browser\Plugin\Field\FieldWidget\EntityReferenceBrowserWidget;

/**
 * Plugin implementation of the 'entity_reference' widget for entity browser.
 *
 * @FieldWidget(
 *   id = "custom_cardinality_entity_browser_entity_reference",
 *   label = @Translation("Custom Cardinality - Entity browser"),
 *   description = @Translation("Uses entity browser to select entities (bridge between field_config_cardinality and entity browser"),
 *   multiple_values = TRUE,
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class CustomCardinalityEntityReferenceBrowserWidget extends EntityReferenceBrowserWidget {

  use CustomCardinalityEntityReferenceBrowserWidgetTrait;

  /**
   * {container}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition): bool {
    $moduleHandler = \Drupal::moduleHandler();
    return $moduleHandler->moduleExists('field_config_cardinality') && parent::isApplicable($field_definition);
  }


}
