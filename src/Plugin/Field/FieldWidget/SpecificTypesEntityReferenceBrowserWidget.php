<?php

namespace Drupal\ipi\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_browser\Plugin\Field\FieldWidget\EntityReferenceBrowserWidget;
use Drupal\paragraphs_library\Entity\LibraryItem;

/**
 * Plugin implementation of the 'entity_reference' widget for entity browser.
 *
 * @FieldWidget(
 *   id = "specific_types_entity_browser_entity_reference",
 *   label = @Translation("Specific types - Entity browser"),
 *   description = @Translation("Uses entity browser to select specific paragraph types in library item"),
 *   multiple_values = TRUE,
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class SpecificTypesEntityReferenceBrowserWidget extends EntityReferenceBrowserWidget {

  public const MODULE_NAME = 'ipi';

  public const SETTINGS_KEY = 'specific_paragraph_types';

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $element = parent::settingsForm($form, $form_state);
    $element['field_widget_display']['#access'] = FALSE;
    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['#attributes']['class'][] = 'field--widget-entity-browser-entity-reference';
    $element['#attached']['library'][] = 'ipi/improveSpecificTypesEntityReferenceBrowserWidget';

    if (!empty($element['current']['items']) && !empty($element['entity_browser']['#default_value'])) {

      foreach ($element['current']['items'] as $index => &$item) {
        $libraryItem = $element['entity_browser']['#default_value'][$index];
        if ($libraryItem instanceof LibraryItem) {
          if (!$libraryItem->isPublished()) {
            $item['#attributes']['class'][] = 'entity-status--unpublished';
          }
        }

        $paragraph = $libraryItem->getParagraph();
        $item['display'] = [
          '#theme' => 'specific_types_entity_reference_browser_widget_container',
          '#label' => $paragraph->label(),
          '#type' => $paragraph->getType(),
          '#title' => $paragraph->buildTitle(),
        ];
      }
    }

    return $element;
  }

  /**
   * {@inheritDoc}
   */
  protected function getPersistentData(): array {
    $persistentData = parent::getPersistentData();
    $ipiSettings = $this->fieldDefinition->getThirdPartySettings(static::MODULE_NAME);
    if (!isset($ipiSettings[static::SETTINGS_KEY])
      || empty($specificParagraphTypes = $ipiSettings[static::SETTINGS_KEY])) {
      return $persistentData;
    }
    $persistentData['widget_context']['target_types'] = $specificParagraphTypes;
    return $persistentData;
  }

}
