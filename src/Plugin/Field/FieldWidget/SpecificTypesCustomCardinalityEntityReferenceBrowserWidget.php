<?php

namespace Drupal\ipi\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Plugin implementation of the 'entity_reference' widget for entity browser.
 *
 * @FieldWidget(
 *   id = "specific_types_custom_cardinality_entity_browser_entity_reference",
 *   label = @Translation("Specific types (with custom cardinality support) - Entity browser"),
 *   description = @Translation("Uses entity browser to select specific paragraph types in library item (with custom cardinality support)"),
 *   multiple_values = TRUE,
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class SpecificTypesCustomCardinalityEntityReferenceBrowserWidget extends SpecificTypesEntityReferenceBrowserWidget {

  use CustomCardinalityEntityReferenceBrowserWidgetTrait;

  /**
   * {container}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition): bool {
    $moduleHandler = \Drupal::moduleHandler();
    return $moduleHandler->moduleExists('field_config_cardinality') && parent::isApplicable($field_definition);
  }

}
