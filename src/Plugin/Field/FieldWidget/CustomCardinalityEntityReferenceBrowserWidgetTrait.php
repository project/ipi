<?php

namespace Drupal\ipi\Plugin\Field\FieldWidget;

/**
 * Trait to add custom cardinality support on multiple widgets.
 *
 * Need #3282258 (to be solved).
 * https://www.drupal.org/files/issues/2024-01-12/3282258-5.patch to use this.
 */
trait CustomCardinalityEntityReferenceBrowserWidgetTrait {

  /**
   * Fetch custom cardinality set in field instance or default cardinality.
   *
   * @return int
   *   Field cardinality.
   */
  protected function fetchFieldCardinality(): int {
    $cardinality = $this->fieldDefinition->getThirdPartySetting('field_config_cardinality', 'cardinality_config');
    $cardinality = (integer) $cardinality;
    if (!$cardinality) {
      return $this->fieldDefinition->getFieldStorageDefinition()->getCardinality();
    }
    return $cardinality;
  }

}
