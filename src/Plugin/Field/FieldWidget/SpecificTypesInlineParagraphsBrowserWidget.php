<?php

/**
 * @file
 * Paragraphs Previewer widget implementation for paragraphs.
 */

namespace Drupal\ipi\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\paragraphs\Plugin\Field\FieldWidget\InlineParagraphsWidget;
use Drupal\paragraphs_browser\Plugin\Field\FieldWidget\InlineParagraphsBrowserWidgetTrait;
use Drupal\paragraphs_browser\Plugin\Field\FieldWidget\ParagraphsBrowserWidget;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Plugin implementation of the 'entity_reference paragraphs' widget.
 *
 * We hide add / remove buttons when translating to avoid accidental loss of
 * data because these actions effect all languages.
 *
 * @FieldWidget(
 *   id = "specific_types_entity_reference_paragraphs_browser",
 *   label = @Translation("Specific Types - Paragraphs Browser Classic"),
 *   description = @Translation("An paragraphs inline form widget with a previewer for specific paragraph types."),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class SpecificTypesInlineParagraphsBrowserWidget extends ParagraphsBrowserWidget {

  /**
   * RequestStack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  use InlineParagraphsBrowserWidgetTrait;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\ipi\Plugin\Field\FieldWidget\SpecificTypesInlineParagraphsBrowserWidget $instance */
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setRequestStack($container->get('request_stack'));
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function formMultipleElements(FieldItemListInterface $items, array &$form, FormStateInterface $form_state) {
    // Silent warning because parent module is not perfect.
    $elements = @parent::formMultipleElements($items, $form, $form_state);
    if (empty($elements['add_more']['browse'])) {
      return $elements;
    }
    $url = &$elements['add_more']['browse']['#ajax']['url'];
    if (!$url instanceof Url) {
      return $elements;
    }
    // Add uuid-parent to pass parameter of EntityBrowser
    // selection storage to ParagraphBrowserForm.
    $url->setRouteParameter('uuid-parent', $this->requestStack->getCurrentRequest()->query->get('uuid'));
    return $elements;
  }

  /**
   * Set requestStack properties.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   RequestStack service.
   */
  public function setRequestStack(RequestStack $requestStack): void {
    $this->requestStack = $requestStack;
  }

}
