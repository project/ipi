<?php

namespace Drupal\ipi\Form;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\FieldConfigInterface;
use Drupal\ipi\Plugin\Field\FieldWidget\SpecificTypesEntityReferenceBrowserWidget;
use Drupal\paragraphs\ParagraphsTypeInterface;

/**
 * Form to configure specific paragraph types for library item reference fields.
 */
class SpecificTypesConfigurationPerParagraphForm extends FormBase {

  public const FORM_ID = 'specific_types_configuration_per_paragraph_form';

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Entity type bundle info manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  /**
   * List of entity reference fields that list library item entities.
   *
   * @var \Drupal\field\FieldConfigInterface[]
   */
  protected array $libraryItemReferenceFields;

  /**
   * ParagraphAttachmentsForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   Entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   Entity type bundle info manager.
   */
  public function __construct(EntityFieldManagerInterface $entityFieldManager,
                              EntityTypeManagerInterface $entityTypeManager,
                              EntityTypeBundleInfoInterface $entityTypeBundleInfo) {
    $this->entityFieldManager = $entityFieldManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return static::FORM_ID;
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ParagraphsTypeInterface $paragraphs_type = NULL): array {
    $definitions = $this->entityTypeManager->getDefinitions();
    $form['fields'] = [
      '#type' => 'tableselect',
      '#header' => [
        'field' => $this->t('Field'),
        'bundle' => $this->t('Bundle'),
        'entity_type' => $this->t('Entity type'),
      ],
      '#options' => [],
      '#default_values' => [],
    ];
    foreach ($definitions as $entityTypeId => $entityType) {
      if (!$entityType->entityClassImplements(FieldableEntityInterface::class)) {
        continue;
      }
      $bundles = $this->entityTypeBundleInfo->getBundleInfo($entityTypeId);
      foreach ($bundles as $bundleId => $bundle) {
        $fields = $this->entityFieldManager->getFieldDefinitions($entityTypeId, $bundleId);
        foreach ($fields as $field) {
          if ($field->getType() !== 'entity_reference' ||
            $field->getSetting('target_type') !== 'paragraphs_library_item') {
            continue;
          }
          if (!$field instanceof FieldConfigInterface) {
            continue;
          }

          $this->libraryItemReferenceFields[$field->id()] = $field;
          $form['fields']['#options'][$field->id()] = [
            'field' => $field->label() . " - " . $field->getName() ,
            'bundle' => $bundle['label'],
            'entity_type' => $entityType->getLabel(),
          ];
          $paragraphsSelected = $field->getThirdPartySetting(
            SpecificTypesEntityReferenceBrowserWidget::MODULE_NAME,
            SpecificTypesEntityReferenceBrowserWidget::SETTINGS_KEY
          );
          if (in_array($paragraphs_type->id(), $paragraphsSelected ?? [])) {
            $form['fields']['#default_value'][$field->id()] = TRUE;
          }
        }
      }
    }
    $form_state->addBuildInfo('paragraphs_type', $paragraphs_type);
    $form['actions'] = [
      '#type' => 'container',
    ];
    $form['actions']['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $paragraphType = $form_state->getBuildInfo()['paragraphs_type'] ?? NULL;
    if (!$paragraphType instanceof ParagraphsTypeInterface) {
      return;
    }
    $fields = $form_state->getValue('fields');
    $fields = array_filter($fields, function ($item) {
      return $item;
    });
    if (empty($this->libraryItemReferenceFields)) {
      return;
    }
    foreach ($fields as $fieldId) {
      $field = $this->libraryItemReferenceFields[$fieldId] ?? NULL;
      if (!$field instanceof FieldConfigInterface) {
        continue;
      }
      $paragraphsSelected = $field->getThirdPartySetting(
        SpecificTypesEntityReferenceBrowserWidget::MODULE_NAME,
        SpecificTypesEntityReferenceBrowserWidget::SETTINGS_KEY
      );
      $paragraphsSelected[$paragraphType->id()] = $paragraphType->id();
      $field->setThirdPartySetting(
        SpecificTypesEntityReferenceBrowserWidget::MODULE_NAME,
        SpecificTypesEntityReferenceBrowserWidget::SETTINGS_KEY,
        $paragraphsSelected);
      $field->save();
    }
  }

}
