<?php

namespace Drupal\ipi\Form;

use Drupal\paragraphs_library\Entity\LibraryItem;
use Drupal\paragraphs_library\Form\LibraryItemForm as BaseLibraryItemForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Extends the LibraryItemForm.
 *
 * This custom form overrides the default LibraryItemForm to introduce custom behavior.
 */
class LibraryItemForm extends BaseLibraryItemForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $paragraph = $form_state->getFormObject()->getEntity();
    if (!$paragraph instanceof LibraryItem) {
      return [];
    }
    if (!$paragraph->isPublished()) {
      $form['#attributes']['class'][] = 'entity-status--unpublished';
    }
    $form['#attached']['library'][] = 'ipi/entityStatus';

    return $form;
  }

}
