<?php

namespace Drupal\ipi\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface;
use Drupal\paragraphs_browser\Entity\BrowserType;
use Drupal\paragraphs_browser\Form\ParagraphsBrowserForm as BaseParagraphsBrowserForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Override base ParagraphsBrowserForm.
 */
class ParagraphsBrowserForm extends BaseParagraphsBrowserForm {

  /**
   * KeyValueStoreExpirable service.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface
   */
  protected KeyValueExpirableFactoryInterface $keyValueExpirableFactory;

  public static function create(ContainerInterface $container): static {
    $instance = parent::create($container);
    $instance->keyValueExpirableFactory = $container->get('keyvalue.expirable');
    $instance->requestStack = $container->get('request_stack');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $field_config = NULL, $paragraphs_browser_type = NULL, $uuid = NULL): array {
    $form = parent::buildForm($form, $form_state, $field_config, $paragraphs_browser_type, $uuid);
    $targetBundles = $this->extractTargetBundles();
    if (!$paragraphs_browser_type instanceof BrowserType) {
      return [];
    }
    $groups = $paragraphs_browser_type->groupManager()->getGroups();
    if (empty($groups)) {
      return $form;
    }
    $itemsRemovedPerGroup = [];
    $itemsPerGroup = [];
    foreach ($groups as $key => $group) {
      $itemsRemovedPerGroup[$key] = 0;
      $itemsPerGroup[$key] = 0;
      foreach ($form['paragraph_types'][$key] as $paragraphType => &$item) {
        if (empty($item['#theme']) || $item['#theme'] !== 'paragraphs_browser_paragraph_type') {
          continue;
        }
        $itemsPerGroup[$key]++;
        if (in_array($paragraphType, $targetBundles)) {
          continue;
        }
        $item['#access'] = FALSE;
        $itemsRemovedPerGroup[$key]++;
      }
      if ($itemsPerGroup[$key] == $itemsRemovedPerGroup[$key]) {
        $form['paragraph_types'][$key]['#access'] = FALSE;
        unset($form['paragraph_types']['filters']['#options'][$key]);
      }
    }
    unset($form['paragraph_types']['filters']['#options']['_na']);
    $form['paragraph_types']["_na"]['#access'] = FALSE;
    return $form;
  }


  /**
   * {@inheritDoc}
   */
  protected function extractTargetBundles(): array {
    $paragraphBrowserType = $this->requestStack->getCurrentRequest()->get('paragraphs_browser_type');
    if (!$paragraphBrowserType instanceof BrowserType) {
      return [];
    }
    $target_bundles = array_keys($paragraphBrowserType->map);
    $uuid = $this->requestStack->getCurrentRequest()->query->get('uuid-parent');
    if (empty($uuid) || !($keyValue = $this->keyValueExpirableFactory->get('entity_browser')) instanceof KeyValueStoreExpirableInterface) {
      return $target_bundles;
    }
    $storage = $keyValue->get($uuid);
    $types = array_values($storage['widget_context']['target_types']);
    return array_intersect($target_bundles, $types);
  }

}
