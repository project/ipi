<?php

namespace Drupal\ipi\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\ipi\Form\ParagraphsBrowserForm;
use Drupal\paragraphs_browser\Controller\ParagraphsBrowserController as BaseParagraphsBrowserController;

/**
 * Override ParagraphsBrowserController.
 */
class ParagraphsBrowserController extends BaseParagraphsBrowserController {

  /**
   * {@inheritdoc}
   */
  public function paragraphsBrowserSelect($field_config, $paragraphs_browser_type, $uuid): AjaxResponse {
    $response = parent::paragraphsBrowserSelect($field_config, $paragraphs_browser_type, $uuid);
    $commands = &$response->getCommands();

    $form = \Drupal::formBuilder()->getForm(ParagraphsBrowserForm::class,
                                            $field_config,
                                            $paragraphs_browser_type,
                                            $uuid);

    $form['#attached']['library'][] = 'paragraphs_browser/modal';
    $title = $this->t('Browse');
    $response = new AjaxResponse();
    $response->addCommand(new OpenModalDialogCommand($title, $form,$commands[0]['dialogOptions']));
    return $response;
  }
}
