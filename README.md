# Installation
### You will need to import views.view.paragraphs_library_browser.yml config file (in optional folder) to make paragraphs scope work.

Even if no patch is needed to install this module, you will need to install those patches for paragraphs_browser module to use ipi.

- **Issue #3247255 - Paragraphs Browser Widget's doesn't work for base fields (like Paragraphs Library item entity) :** Use 3247255-9.patch.
- **Issue #3340467 - Replace deprecated jquery/once with core/once :** Use paragraphs_browser-core_once-3340467-18.patch
- **Issue #3446022 - PHP 8.2 : Compatiblity :** Use 3446022-2.patch

If you will use media library in paragraphs library item, you will need this patch for drupal core

- **Issue #2741877 - Nested modals don't work: opening a modal from a modal closes the original :** Use 2741877-91-workaround-10.1.patch

# Configuration

Let's assume you already created multiple paragraphs type.

- In library item form display, configure "Paragraphs" field to use widget "Specific Types - Paragraphs Browser Classic"
- In your entity (it can be any type, such as node but also paragraphs), create a field that reference entity
(not entity reference revisions).
- In this field, you will be able to configure which paragraphs type you want to configure.
- In your entity form field, for your new field, select "Specific types - Entity browser" as field widget.
Be careful, even if you have one entity browser, you have to select your entity browser in widget configuration.

# Extra

- You will find two configurations into config/optional.
This will allow you to :
  - Add a new tab in paragraph browser to add paragraphs library item inside modal.
  - Add operation links to edit paragraphs library items without leaving modal.
